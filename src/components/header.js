import React from 'react'
import Link from 'gatsby-link'

import RoutedNavbar from './navbar'


const Header = (props={}) => (
  <div className="hero" color="primary">
    <div className="hero-body">
      <h1 className='is-size-1 title'>
        <Link to="/" > {props.siteTitle}  </Link>
      </h1>
    </div>
    <div className="hero-footer">

      <RoutedNavbar {...props} />

    </div>
  </div>
)

export default Header
